def part_1
	pairs = File.new('input.txt').read.split("\n")
	pairs.sum do |pair|
		opponent, you = pair.split(' ').map(&method(:to_shape))
		score(opponent, you)
	end
end

# @param [String] symbol
# @return [Symbol]
def to_shape(symbol)
	return :rock if ['A', 'X'].include? symbol
	return :paper if ['B', 'Y'].include? symbol
	return :scissors if ['C', 'Z'].include? symbol
end

# @param [Symbol] opp
# @param [Symbol] you
# @return [Symbol]
def outcome(opp, you)
	return :draw if opp == you
	return :win if (you == :rock && opp == :scissors) || (you == :paper && opp == :rock) || (you == :scissors && opp == :paper)
	:loss
end

# @param [Symbol] opp
# @param [Symbol] you
# @return [Integer]
def score(opp, you)
	shape_score = case you
								when :rock
									1
								when :paper
									2
								when :scissors
									3
								else
									0
								end

	outcome_score = case outcome(opp, you)
									when :win
										6
									when :loss
										0
									when :draw
										3
									else
										0
									end

	outcome_score + shape_score
end

puts part_1
