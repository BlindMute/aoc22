def main
	backpacks = File.new('input.txt').read.split("\n")
	backpacks.sum do |b|
		left, right = b.split('').partition.with_index { |_, index| index < b.length / 2 }
		priority((left & right).first)
	end
end

def main2
	backpacks = File.new('input.txt').read.split("\n")
	backpacks.each_slice(3).sum do |group|
		priority group
							 .map { |b| b.split('') }
							 .reduce(:intersection)
							 .first
	end
end

# @param [String] letter
# @return [Integer]
def priority(letter)
	if letter.downcase == letter
		letter.ord - 96
	else
		letter.ord - 38
	end
end

puts main, main2
