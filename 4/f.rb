def range_pairs
	pairs = File.new('input.txt').read.split("\n")

	def string_to_range(range_str)
		# "12-34"
		left, right = range_str.split('-')
		(left.to_i..right.to_i)
	end

	pairs.map do |pair|
		left, right = pair.split(',')
		[string_to_range(left), string_to_range(right)]
	end
end

def contains?(range1, range2)
	(range1.include?(range2.first) && range1.include?(range2.last)) ||
		(range2.include?(range1.first) && range2.include?(range1.last))
end

def overlap?(range1, range2)
	range1.include?(range2.first) || range1.include?(range2.last) ||
		range2.include?(range1.first) || range2.include?(range1.last)
end

p range_pairs.count { |pair| contains?(*pair) }
p range_pairs.count { |pair| overlap?(*pair) }
