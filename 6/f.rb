signal = File.new('input.txt').read.split('')
num_chr = 14
p signal.find_index.with_index { |_, i| signal[i...i + num_chr].uniq.size == num_chr } + num_chr
