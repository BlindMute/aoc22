def part_2
	pairs = File.new('input.txt').read.split("\n")
	pairs.sum do |pair|
		opp_string, desired_outcome = pair.split(' ')
		opp_shape = to_shape(opp_string)
		your_shape = shape_from_outcome(opp_shape, desired_outcome)
		your_shape.vs_score(opp_shape) + your_shape.score
	end
end

class Shape
	def self.vs_score(other)
		return 6 if self.beats == other
		return 0 if other.beats == self
		3
	end
end

class Rock < Shape
	class << self
		def beats
			Scissors
		end

		def score
			1
		end
	end
end

class Paper < Shape
	class << self
		def beats
			Rock
		end

		def score
			2
		end
	end

end

class Scissors < Shape
	class << self
		def beats
			Paper
		end

		def score
			3
		end
	end
end

def to_shape(shape_string)
	return Rock if shape_string == 'A'
	return Paper if shape_string == 'B'
	return Scissors if shape_string == 'C'
end

def shape_from_outcome(opponent, outcome)
	return opponent if outcome == 'Y'
	return opponent.beats if outcome == 'X'
	return opponent.beats.beats if outcome == 'Z'
end

puts part_2
