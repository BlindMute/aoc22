commands = File.new('input.txt').read.split("\n")
commands.filter! { |line| line.start_with? 'move' }

stacks = [
	%w{S L W},
	%w{J T N Q},
	%w{S C H F J},
	%w{T R M W N G B},
	%w{T R L S D H Q B},
	%w{M J B V F H R L},
	%w{D W R N J M},
	%w{B Z T F H N D J},
	%w{H L Q N B F T},
]

commands.each do |c|
	amount, from, to = c.scan(/\d+/).map(&:to_i)

	#	part 1
	# 		amount.times do
	# 			stacks[to - 1].push(stacks[from - 1].pop)
	# 		end

	# part 2
	stacks[to - 1].push(*stacks[from - 1].pop(amount))
end

p stacks.map(&:last).join

