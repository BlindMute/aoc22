def part_1
	File.new('input.txt').read
		&.split("\n\n")
		.map { |elf| elf.split("\n").sum(&:to_i) }
		.max
end

def part_2
	File.new('input.txt').read
		&.split("\n\n")
		.map { |elf| elf.split("\n").sum(&:to_i) }
		.max(3)
		.sum
end

puts part_1, part_2
